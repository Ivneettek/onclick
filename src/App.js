import React, { useEffect } from 'react';
import Amplify, {API} from 'aws-amplify' 
import config from './aws-exports'
import {withAuthenticator, AmplifySignOut} from '@aws-amplify/ui-react';

import './App.css';


Amplify.configure(config);

function App() {
  
  const[userName, setUserName] = React.useState('');
  const[users, setUsers] = React.useState([]);
  

  useEffect(() => {
    API.get("oneclick", "/oneclick/name").then((oneRes) => {
      setUsers([...users, ...oneRes]);
    });
   },[]);

   const handleSubmit = e => {
    e.preventDefault()
    API.post('oneclick', '/one' , {
     body: {
       name: userName
     },
    }).then(() => {
      setUsers([...users, {name: userName}])
    });
  };

  return (
    <div className="App">
      <header className="App-header">
         Hello
         <form onSubmit={handleSubmit}> 
           <input value = {userName} placeholder="TypeYourName  " onChange ={(e) => setUserName(e.target.value)}/>
           <button> Add User </button>

           </form>

           <ul>

{users.map(user => <li> {user.name} </li> )}
      </ul>
     <AmplifySignOut/>
         <AmplifySignOut/>
        
      </header>
    </div>
  );
}

export default withAuthenticator(App);
